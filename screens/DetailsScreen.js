import React, {Component} from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

class DetailsScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
            <Text style={styles.title} >
              Gubaton, John Eric Paolo R. {"\n"}
              Male {"\n"}
              22 Years Old {"\n"}
              Current Studying in the course of BSIT
            </Text>
            <Button
              title="Home"
              onPress={() => this.props.navigation.navigate('Home')}
            />
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
      margin: 10,
    },
    title: {
      fontSize: 20,
      margin: 20,
      textAlign: 'center',
    },
  });

export default DetailsScreen;
