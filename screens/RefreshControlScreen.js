import React, { Component } from 'react';
import { StyleSheet, View, ListView, RefreshControl, Text, Button } from 'react-native'

export default class RefreshControlScreen extends Component {
    constructor() {
        super()
        this.state = {
            refreshing: false,
            dataSource: new ListView.DataSource({
                rowHasChanged: (row1, row2) => row1 !== row2
            }),
            rest: [
                { sleep: 'Sleep', day: 'Monday' , status: 'None'},
            ]
        }
    }

    componentWillMount() {
        this.setState({
            dataSource:
                this.state.dataSource.cloneWithRows(this.state.rest)
        })
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <ListView
                    refreshControl={this._refreshControl()}
                    dataSource={this.state.dataSource}
                    renderRow={(rest) => this._renderListView(rest)}>
                </ListView>
                <Button style={styles.space}
                  title="Home"
                  onPress={() => this.props.navigation.navigate('Home')}
              />
            </View>
        )
    }

    _renderListView(rest) {
        return (
            <View style={styles.listView}>
                <Text>{rest.sleep}</Text>
                <Text>{rest.day}</Text>
                <Text>{rest.status}</Text>
            </View>
        )
    }

    _refreshControl() {
        return (
            <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={() => this._refreshListView()} />
        )
    }

    _refreshListView() {
        //Start Rendering Spinner
        this.setState({ refreshing: true })
        this.state.rest.push(
            { sleep: 'Sleep', day: 'Tuesday' , status: 'None'},
            { sleep: 'Sleep', day: 'Wednesday' , status: 'None'},
            { sleep: 'Sleep', day: 'Thursday' , status: 'None'},
            { sleep: 'Sleep', day: 'Friday' , status: 'None'},
            { sleep: 'Sleep', day: 'Saturday' , status: 'Hell Yeah!'},
            { sleep: 'Sleep', day: 'Sunday' , status: 'A Little'},
        )
        //Updating the dataSource with new data
        this.setState({
            dataSource:
                this.state.dataSource.cloneWithRows(this.state.rest)
        })
        this.setState({ refreshing: false }) //Stop Rendering Spinner
    }

}

const styles = StyleSheet.create({

    listView: {
        flex: 1,
        backgroundColor: '#fff',
        marginTop: 10,
        marginRight: 10,
        marginLeft: 10,
        padding: 10,
        borderWidth: .5,
        borderColor: '#dddddd',
        height: 70
    },

    space: {
        margin: 20,
    },

});