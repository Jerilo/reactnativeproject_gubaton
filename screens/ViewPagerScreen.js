import React, { Component } from 'react';
import { ViewPagerAndroid, View, Text, StyleSheet, Button } from 'react-native';

export default class ViewPagerScreen extends Component {
    render() {
        return (
            <ViewPagerAndroid
                style={styles.viewPager}
                initialPage={0}>
                <View style={styles.pageStyle} key="1">
                    <Text style={[styles.text]}>Unopened Surprise</Text>
                    <Text style={[styles.text]}> Swipe left</Text>
                </View>
                <View style={styles.pageStyle} key="2">
                    <Text style={[styles.text]}>Surprise!</Text>
                    <Text style={[styles.text]}>Swipe right</Text>
                    <Button style={styles.space}
                        title="Home"
                        onPress={() => this.props.navigation.navigate('Home')}
                    />
                </View>
            </ViewPagerAndroid>
        )
    }
}

const styles = StyleSheet.create({
    viewPager: {
        flex: 1
    },
    pageStyle: {
        alignItems: 'center',
        padding: 20,
    },
    text: {
        fontSize: 15,
        fontWeight: 'bold',
    }
});
