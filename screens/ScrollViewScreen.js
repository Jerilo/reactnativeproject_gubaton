import React, { Component } from 'react';
import { Text, View, StyleSheet, ScrollView, Button } from 'react-native';
import { white } from 'ansi-colors';

export default class ScrollViewScreen extends Component {
   state = {
      names: [
         {'name': 'Gluttony', 'id': 1},
         {'name': 'Sloth', 'id': 2},
         {'name': 'Greed', 'id': 3},
         {'name': 'Lust', 'id': 4},
         {'name': 'Pride', 'id': 5},
         {'name': 'Wrath', 'id': 6},
         {'name': 'Envy', 'id': 7},
         {'name': 'Temperance', 'id': 8},
         {'name': 'Diligence', 'id': 9},
         {'name': 'Charity', 'id': 10},
         {'name': 'Chastiy', 'id': 11},
         {'name': 'Humility', 'id': 12},
         {'name': 'Forgiveness', 'id': 13},
         {'name': 'Kindess', 'id': 14}
      ]
   }
   render() {
      return (
         <View>
            <ScrollView>
               {
                  this.state.names.map((item, index) => (
                     <View key = {item.id} style = {styles.item}>
                        <Text style={[styles.text]}>{item.name}</Text>
                     </View>
                  ))
               }
               <View style={{height: 15, backgroundColor: 'white'}} />
               <Button
                    title="Home"
                    onPress={() => this.props.navigation.navigate('Home')}
                />
            </ScrollView>
            
         </View>
      )
   }
}

const styles = StyleSheet.create ({
   item: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: 30,
      margin: 2,
      borderColor: '#2a4944',
      borderWidth: 1,
      backgroundColor: 'gray'
   },
   text: {
    fontSize: 15,
    textAlign: 'center',
    fontWeight: 'bold',
    color: 'white',
   }
});