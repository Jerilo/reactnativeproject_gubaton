import React, {Component} from 'react';
import { StyleSheet, Text, View, Button, ScrollView } from 'react-native';

class HomeScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
              <ScrollView>
              <Text style={styles.title}>Home Screen</Text>

              <Button
                color = "gray"
                title="Details"
                onPress={() => this.props.navigation.navigate('Details')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                color = "gray"
                title="Activity Indicator"
                onPress={() => this.props.navigation.navigate('ActivityIndicator')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                color = "gray"
                title="Drawer Layout"
                onPress={() => this.props.navigation.navigate('DrawerLayout')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                color = "gray"
                title="Image Screen"
                onPress={() => this.props.navigation.navigate('ImageScreen')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                color = "gray"
                title="Keyboard Avoid Screen"
                onPress={() => this.props.navigation.navigate('KeyboardAvoid')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                color = "gray"
                title="List View"
                onPress={() => this.props.navigation.navigate('ListView')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                color = "gray"
                title="Modal"
                onPress={() => this.props.navigation.navigate('ModalScreen')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                color = "gray"
                title="Picker Screen"
                onPress={() => this.props.navigation.navigate('PickerScreen')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button 
                  color = "gray"
                  title="Progress Bar"
                  onPress={() => this.props.navigation.navigate('ProgressBar')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                  color = "gray"
                  title="Refresh Control"
                  onPress={() => this.props.navigation.navigate('RefreshControl')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                  color = "gray"
                  title="Scroll View"
                  onPress={() => this.props.navigation.navigate('ScrollView')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                  color = "gray"
                  title="Section List"
                  onPress={() => this.props.navigation.navigate('SectionList')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                  color = "gray"
                  title="Slider"
                  onPress={() => this.props.navigation.navigate('Slider')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                  color = "gray"
                  title="Status Bar"
                  onPress={() => this.props.navigation.navigate('StatusBar')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                  color = "gray"
                  title="Switch Screen"
                  onPress={() => this.props.navigation.navigate('Switch')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                  color = "gray"
                  title="Text Input"
                  onPress={() => this.props.navigation.navigate('TextInput')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                  color = "gray"
                  title="Text Screen"
                  onPress={() => this.props.navigation.navigate('Text')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                  color = "gray"
                  title="Touchable HighLight"
                  onPress={() => this.props.navigation.navigate('TouchableHighlight')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                  color = "gray"
                  title="Touchable Native Feedback"
                  onPress={() => this.props.navigation.navigate('TouchableNativeFeedback')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                  color = "gray"
                  title="Touchable Opacity"
                  onPress={() => this.props.navigation.navigate('TouchableOpacity')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                  color = "gray"
                  title="View Pager Android"
                  onPress={() => this.props.navigation.navigate('ViewPager')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                  color = "gray"
                  title="View Screen"
                  onPress={() => this.props.navigation.navigate('View')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />

              <Button
                  color = "gray"
                  title="Web View"
                  onPress={() => this.props.navigation.navigate('WebView')}
              />
              <View style={{height: 20, backgroundColor: 'white'}} />
              </ScrollView>
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    title: {
      fontSize: 20,
      margin: 20,
      textAlign: 'center',
    },
  });

export default HomeScreen;
