import React, { Component } from 'react';
import { View, ListView, Text, Button, StyleSheet } from 'react-native';

export default class ListViewScreen extends Component {
    constructor() {
        super();
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            dataSource: ds.cloneWithRows(['Passed', 'Failed', 'Incomplete', 'Unauthorized Withrawal', 'Dropped']),
        };
    }

    render() {
        return (
            <View>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={(rowData) => <Text style={[styles.textList]}>{rowData}</Text>}
                />
                <Button
                    title="Home"
                    onPress={() => this.props.navigation.navigate('Home')}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    textList: {
      fontSize: 15,
      marginBottom: 10,
      textAlign: 'left',
    },
  })