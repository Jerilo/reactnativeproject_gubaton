import React, { Component } from 'react';
import { SectionList, Text, View, Button, StyleSheet } from 'react-native';

export default class SectionListScreen extends Component {
    render() {
        return (
            <View>
                <SectionList
                renderItem={({ item, index, section }) => <Text key={index}>{item}</Text>}
                renderSectionHeader={({ section: { title } }) => (
                    <Text style={{ fontWeight: 'bold' }}>{title}</Text>
                )}
                sections={[
                    { title: 'Full Name', data: ['Gubaton', 'John Eric Paolo', 'Regalado'] },
                    { title: 'Address', data: ['Km. Waling - Waling Street', 'Buhangin', 'Davao City', '8000'] },
                    { title: 'Education', data: ['Primary: University of Immaculate Conception', 'Secondary: Davao City National Highschool', 'Tertiary: University of SouthEastern Philippines'] },
                ]}
                keyExtractor={(item, index) => item + index}
            />
            <Button style={styles.space}
                  title="Home"
                  onPress={() => this.props.navigation.navigate('Home')}
              />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    space: {
      margin: 20,
    }
  });