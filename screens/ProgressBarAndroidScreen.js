import React, { Component } from "react";
import {
    ProgressBarAndroid,
    StyleSheet,
    View,
    Button,
    Text
} from "react-native";

export default class ProgressBarAndroidScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={[styles.text]}>Please Wait.. Loading some Sleep...</Text>
                <ProgressBarAndroid />
                <Text style={[styles.text]}>Still.. Loading...</Text>  
                <ProgressBarAndroid styleAttr="Horizontal" />
                <Text style={[styles.text]}>Umm.. Loading...  ?</Text>
                <ProgressBarAndroid styleAttr="Horizontal" color="#2196F3" />
                <Text style={[styles.text]}>Sleep has ceased to Load. Please try again tomorrow.</Text>
                <ProgressBarAndroid
                    styleAttr="Horizontal"
                    indeterminate={false}
                    progress={0.5}
                />
                <Button
                    title="Home"
                    onPress={() => this.props.navigation.navigate('Home')}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "space-evenly",
        padding: 10
    },
    text: {
        fontSize: 15,
        textAlign: 'center',
    }
});
