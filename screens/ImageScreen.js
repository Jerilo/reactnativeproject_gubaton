import React, { Component } from 'react';
import { View, Image, Button, Text } from 'react-native';

export default class ImageScreen extends Component {
    render() {
        return (
            <View style={{
                justifyContent: 'center',
                alignItems: 'center',
            }}>

                <Text>This is a Sample Image #1 </Text>
                <View style={{height: 15, backgroundColor: 'white'}} />
                <Image
                    style={{ width: 50, height: 50 }}
                    source={{ uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png' }}
                />
                <View style={{height: 20, backgroundColor: 'white'}} />

                <Text>This is a Sample Image #2 </Text>
                <View style={{height: 15, backgroundColor: 'white'}} />
                <Image
                    style={{ width: 66, height: 58 }}
                    source={{ uri: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAAzCAYAAAA6oTAqAAAAEXRFWHRTb2Z0d2FyZQBwbmdjcnVzaEB1SfMAAABQSURBVGje7dSxCQBACARB+2/ab8BEeQNhFi6WSYzYLYudDQYGBgYGBgYGBgYGBgYGBgZmcvDqYGBgmhivGQYGBgYGBgYGBgYGBgYGBgbmQw+P/eMrC5UTVAAAAABJRU5ErkJggg==' }}
                />
                <View style={{height: 20, backgroundColor: 'white'}} />

                <Button
                    title="Home"
                    onPress={() => this.props.navigation.navigate('Home')}
                />
            </View>
        );
    }
}
